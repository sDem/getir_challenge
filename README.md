# getir_challenge

An example of Restful API with a single endpoint. Developed with express.js, mongoose, jest and supertest.

## How to install and run
---

```bash
request https://infinite-badlands-50177.herokuapp.com/records or
---
$ git clone https://gitlab.com/sDem/getir_challenge.git
$ cd getir_challenge
$ npm install
$ npm start
$ npm test
```

## Endpoints and Outputs

### POST /records

**Request Body**

| Field     | Type   | Description                       |
| --------- | ------ | --------------------------------- |
| startDate | string | contains date *YYYY-MM-DD* format |
| endDate   | string | contains date *YYYY-MM-DD* format |
| minCount  | number | for filtering counts              |
| maxCount  | number | for filtering counts              |

**Response Body**

```json
{
	"code":0,  
	"msg":"Success",  
	"records":[{  
		"key":"TAKwGc6Jr4i8Z487",  
		"createdAt":"2017-01-28T01:22:14.398Z",  
		"totalCount":2800
	},
	{  
		"key":"NAeQ8eX7e5TEg7oH",  
		"createdAt":"2017-01-27T08:19:14.135Z",  
		"totalCount":2900  
	}]
}
```

**Example Request**

``` bash
curl -X POST https://infinite-badlands-50177.herokuapp.com/records \
	-H 'Content-Type : application/json' \
	-d '{"startDate": "2016-11-26", "endDate": "2018-01-05", "minCount": 2400,            "maxCount": 3000}'
```

Returns 200 and response object. 

