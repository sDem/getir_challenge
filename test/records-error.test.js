/* eslint-disable no-undef */
const app = require('../src/app');
const request = require('supertest');

describe("POST /records", () => {

    describe("when maxCount smaller than minCount", () => {
        test("should return a 400 status code with 002 error code", async () => {
            await request(app).post('/records').send({
                startDate: "2019-01-26",
                endDate: "2018-02-20",
                minCount: 2500,
                maxCount: 2000
            }).then((response) => {
                expect(response.statusCode).toBe(400);
                expect(response.body.code).toBe("002");
            });
        });
    });

    describe("when endDate type not iso date", () => {
        test("should return 400 with 002 error code", async () => {
            await request(app).post('/records').send({
                startDate: "2016-01-26",
                endDate: "20897878",
                minCount: 1000,
                maxCount: 2000
            }).then((response) => {
                expect(response.statusCode).toBe(400);
                expect(response.body.code).toBe("002");
            });
        });
    });

    describe("when filter returns success", () => {
        test("should return code 0 with status code 200 and records", async () => {
            await request(app).post('/records').send({
                startDate: "2016-01-26",
                endDate: "2018-02-25",
                minCount: 1000,
                maxCount: 2000
            }).then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body.code).toBe("0");
                expect(response.body.records).toBeTruthy()
            });
        });
    });

});