const express = require('express');
const Joi = require('joi');
const router = express.Router();
const { handleResponse } = require('../middlewares/responseHandler');
const { ErrorHandler } = require('../middlewares/errorHandler')
const recordService = require('../controller/recordService');


router.post('/', (req, res, next) => {
    const { error } = validateRecord(req.body);
    if (error) throw new ErrorHandler(400, '002', error.message);

    let { startDate, endDate, minCount, maxCount } = req.body;
    
    startDate = new Date(startDate);
    endDate = new Date(endDate);

    if (startDate > endDate) throw new ErrorHandler(400, '002', 'validation error : endDate must be bigger');
    if (minCount > maxCount) throw new ErrorHandler(400, '002', 'validation error : maxCount must be bigger');

    recordService.getRecords(startDate, endDate, minCount, maxCount)
        .then((records) => {
            const response = handleResponse(records);
            res.status(200).json(response);
        })
        .catch((recordError) => {
            next(recordError);
        });
});

/**
 * validate record object 
 * 
 * @param {Object} recordObject : recordObject 
 * @returns {Object} : returns validation errors
 */
const validateRecord = (recordObject) => {
    const recordSchema = Joi.object({
        startDate: Joi
            .date()
            .iso()
            .required(),
        endDate: Joi
            .date()
            .iso()
            .required(),
        minCount: Joi
            .number()
            .required(),
        maxCount: Joi
            .number()
            .required()
    });

    return recordSchema.validate(recordObject);
}

module.exports = router;