const mongoose = require('mongoose');

// mongoose schema of records collection
const recordSchema = new mongoose.Schema({
    key : {
        type : String
    },
    value : {
        type : String
    },
    createdAt : {
        type : Date
    },
    counts : {
        type : Array
    }
});

const RecordModel = mongoose.model('records', recordSchema);

module.exports = RecordModel;