const config = require("./config/default");
const express = require("express");
const helmet = require("helmet");

const { handleLog } = require('./middlewares/logHandler');
const { handleError } = require('./middlewares/errorHandler');
const db = require("./controller/dbConnection");

const app = express();

db.on("error", console.error.bind(console, "Mongodb conneciton error:"));

const records = require("./api/record");

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(helmet());
app.use(function (req, res, next) {
    handleLog(req, res, next);
});

app.use("/records", records);

app.use(function (err, req, res, next) {
    handleError(err, res);
});

app.use(function (req, res, next) {
    res.status(400).send({ code: "001", msg: 'Request not found!' });
});

const PORT = config.PORT;
module.exports = app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});