const RecordModel = require("../models/recordModel");

/**
 * Filter records based on client inputs
 * to sum counts a bit pricey on every request
 * it can be cache with redis or store another place
 * 
 * @param {ISODate} startDate : initial value of createdAt filter
 * @param {ISODate} endDate : end value of createdAt filter
 * @param {number} minCount : initial value of count range 
 * @param {number} maxCount : end value of counts range
 * @returns {Object} filtered records
 */
async function getRecords(startDate, endDate, minCount, maxCount) {
    const records = await RecordModel.aggregate([
        {
            $project: {
                _id: 0,
                key: 1,
                createdAt: 1,
                totalCount: { $sum: "$counts" },
            },
        },
        {
            $match: {
                totalCount: { $gte: minCount, $lt: maxCount },
                createdAt: { $gte: startDate, $lt: endDate }
            }
        }
    ]);

    return records;
}

module.exports = {
    getRecords,
};
