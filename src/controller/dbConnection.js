const mongoose = require('mongoose');
const config = require('../config/default');

// mongoDB atlas connection.
mongoose.connect(config.mongoURI, {useNewUrlParser: true,  useUnifiedTopology: true})
    .then(console.log('Connecting to MongoDB Server...'));

const db = mongoose.connection;

module.exports = db;