module.exports = {
    // logs request time, method an path
    handleLog : (req, res, next) => {
        let time = new Date().toLocaleTimeString();
        console.log(`${time} - [${req.method}] - ${req.path}` );
        next();
    }
} 
