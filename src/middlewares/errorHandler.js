class ErrorHandler extends Error {
    constructor(statusCode, errorCode, errorMessage){
        super();
        this.statusCode = statusCode;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
}

const handleError = (err, res) => {
    const { statusCode, errorCode, errorMessage } = err;
    res.status(statusCode).json({
        code : errorCode,
        msg : errorMessage
    });
}

module.exports = {
    ErrorHandler,
    handleError
}